import argparse
import socket
import threading
import warnings
import time
from datetime import datetime
import random
import string

from Crypto.PublicKey import RSA

import searching
import verifying


class ChatServer:
    on_connected = None

    def __init__(self, login, password):
        self._login = login
        self._user = verifying.UserAuthenticator(login, password)

        self._output_socks = {}
        self._connections = []
        self._visible_thread = None
        self._input_connection_thread = None

        self._stopped = True

    def run(self):
        if self._stopped:
            self._visible_thread = searching.make_visible(self._user.uid, self._on_asked_to_connect)

            self._input_connection_thread = _InputConnectionThread(self._on_input_connection)
            self._input_connection_thread.start()

            self._stopped = False

    def stop(self):
        if not self._stopped:
            self._stopped = True

            self._visible_thread.stop()
            self._input_connection_thread.stop()

            self._visible_thread = None
            self._input_connection_thread = None

            for connection in self._connections:
                connection.close()

    def _on_input_connection(self, input_sock, input_addr, listen_port):
        try:
            output_port = int(input_sock.recv(1024))
        except ValueError:
            return

        output_addr = (input_addr[0], output_port)
        output_sock = self._get_or_create_output_socket(output_addr, listen_port)

        connection = _ChatSecureConnection(input_sock, output_sock, self._user)
        self._connections.append(connection)
        if self.on_connected is not None:
            self.on_connected(connection)

    def _get_or_create_output_socket(self, addr, listen_port):
        if addr in self._output_socks:
            sock = self._output_socks[addr]
            del self._output_socks[addr]
        else:
            sock = socket.socket()
            sock.connect(addr)
            sock.send(str(listen_port))
        return sock

    def ask_to_connect(self, uid):
        searching.search(uid, self._input_connection_thread.port)

    def _on_asked_to_connect(self, addr):
        output_sock = socket.socket()
        self._output_socks[addr] = output_sock

        output_sock.connect(addr)
        output_sock.send(str(self._input_connection_thread.port))

    @property
    def login(self):
        return self._login

    @property
    def uid(self):
        return self._user.uid


class _InputConnectionThread(threading.Thread):
    def __init__(self, conn_func):
        super(_InputConnectionThread, self).__init__(target=self._loop)

        self._init_socket()
        self._conn_func = conn_func

        self._stopped = False

    def _loop(self):
        while True:
            if self._stopped:
                break

            try:
                input_sock, input_addr = self._sock.accept()
            except socket.timeout:
                continue
            old_port = self.port
            self._init_socket()
            threading.Thread(target=self._conn_func, args=(input_sock, input_addr, old_port)).start()

    def _init_socket(self):
        self._sock = s = socket.socket()
        s.settimeout(1)
        s.bind(('', 0))
        self._port = s.getsockname()[1]
        self._sock.listen(1)

    @property
    def port(self):
        return self._port

    def stop(self):
        self._stopped = True


class _ChatSecureConnection:
    on_message_received = None
    on_disconnected = None

    def __init__(self, input_sock, output_sock, user):
        self._user = user

        self._key = RSA.generate(1024)
        self._init_commands(input_sock, output_sock)

        self._opened = True

        self._get_public_key()
        self._ask_user_info()
        self._verify_user()

    def _init_commands(self, input_sock, output_sock):
        self._connector = conn = _SocketConnector(input_sock, output_sock)
        conn.on_disconnected = self._on_disconnected

        conn.on_command('message', self._message_cmd)
        conn.on_command('user_info', self._ask_user_info_cmd)
        conn.on_command('get_public_key', self._get_public_key_cmd)
        conn.on_command('sign', self._sign_cmd)

        conn.run()

    def send_message(self, text):
        text = str(text)
        encrypted = self._encode_key.encrypt(text, str(datetime.now()))[0]
        self._connector.send_command('message', encrypted)

    def _message_cmd(self, params):
        if self.on_message_received is not None:
            text = self._key.decrypt(params[0])
            self.on_message_received(text)

    def _ask_user_info(self):
        result = self._connector.send_command('user_info', [])
        self._client_uid = result[0]
        self._client_login = result[1]

    def _ask_user_info_cmd(self, _):
        return [self._user.uid, self._user.login]

    def _get_public_key(self):
        result = self._connector.send_command('get_public_key', [])
        key_str = result[0]
        self._encode_key = RSA.importKey(key_str)

    def _get_public_key_cmd(self, _):
        return [self._key.publickey().exportKey('PEM')]

    def _verify_user(self):
        test_str = ''.join(random.choice(string.ascii_letters + string.digits) for _ in xrange(50))
        result = self._connector.send_command('sign', [test_str])

        if verifying.verify_user(self._client_uid, result[0], test_str, (long(result[1]),)):
            print 'user "{}" verified'.format(self._client_login)
        else:
            print 'user "{}" not verified, connection closed'.format(self._client_login)
            self.close()

    def _sign_cmd(self, params):
        signature = self._user.sign(params[0])[0]
        return [self._user.public_key, signature]

    def _on_disconnected(self):
        if self.on_disconnected is not None:
            self.on_disconnected(self)
            self._opened = False

    @property
    def client_uid(self):
        return self._client_uid

    @property
    def client_login(self):
        return self._client_login

    def __str__(self):
        return str(self._connector)

    def close(self):
        if self._opened:
            self._opened = False
            self._connector.stop()


class _SocketConnector:
    on_disconnected = None

    def __init__(self, input_sock, output_sock):
        self._input_sock = input_sock
        self._output_sock = output_sock

        self._output_sock.settimeout(1)

        self._commands = {'empty': lambda: None}
        self._input_listener_thread = None
        self._opened = False

    def run(self):
        self._input_listener_thread = t = _InputSocketListener(self._input_sock)
        t.on_data = self._on_input_data
        t.on_disconnected = self._on_disconnected
        t.start()

        self._opened = True

    def _on_disconnected(self):
        if self.on_disconnected is not None:
            self.on_disconnected()

        if self._opened:
            self._opened = False
            self._input_sock.close()
            self._output_sock.close()

    def send_command(self, cmd, params):
        if not self._opened:
            return

        if params is None:
            params = []
        if not isinstance(params, list):
            params = [params]
        data = self._command_to_data([cmd] + params)

        try:
            self._output_sock.send(data)
            result_data = self._output_sock.recv(1024)
        except socket.error:
            self._opened = False
            return

        result = self._data_to_command(result_data)
        return result

    def on_command(self, cmd, func):
        self._commands[cmd] = func

    def _on_input_data(self, data):
        input = self._data_to_command(data)
        cmd, params = input[0], input[1:]

        result = self._receive_command(cmd, params)
        result_data = self._command_to_data(result)
        if len(result_data) == 0:
            result_data = 'empty'
        self._input_sock.send(result_data)

    def _receive_command(self, cmd, params):
        if cmd not in self._commands:
            warnings.warn('unknown command: {}'.format(repr(cmd)))
            return
        func = self._commands[cmd]
        return func(params)

    @staticmethod
    def _data_to_command(data):
        data = data.split('\n')
        return [item.decode('string_escape') for item in data]

    @staticmethod
    def _command_to_data(params):
        if params is None:
            return ''

        if not isinstance(params, list):
            params = [params]
        return '\n'.join(str(param).encode('string_escape') for param in params)

    def stop(self):
        if self._opened:
            self._opened = False
            self._input_listener_thread.stop()
            self._input_sock.close()
            self._output_sock.close()

    def __str__(self):
        try:
            input_addr = self._input_sock.getsockname()
            output_addr = self._output_sock.getsockname()
            return '[input: {}, output: {}]'.format(input_addr, output_addr)
        except:
            return '[input: -, output: -]'


class _InputSocketListener(threading.Thread):
    on_disconnected = None
    on_data = None

    def __init__(self, sock):
        super(_InputSocketListener, self).__init__(target=self._loop)

        self._stopped = False
        self._sock = sock

        self._sock.settimeout(1)

    def _loop(self):
        while True:
            if self._stopped:
                break
            try:
                data = self._sock.recv(1024)
                if len(data) == 0:
                    self._disconnected()
                    break
                self._data(data)
            except socket.timeout:
                continue

    def _disconnected(self):
        if not self._stopped:
            self._stopped = True
            if self.on_disconnected is not None:
                threading.Thread(target=self.on_disconnected).start()

    def _data(self, data):
        pass
        if self.on_data is not None:
            threading.Thread(target=self.on_data, args=(data,)).start()

    def stop(self):
        self._stopped = True


def test():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--login', default='user')
    parser.add_argument('-p', '--password', default='123456')

    args = parser.parse_args()

    login = args.login
    password = args.password

    def show_message(text):
        print 'income message: "{}"'.format(text)

    def disconnected(connection):
        print 'disconnected', connection

    def on_connected(connection):
        print 'connected', connection

        connection.on_message_received = show_message
        connection.on_disconnected = disconnected

        time.sleep(1)
        connection.send_message('Hello! I am {}'.format(login))

    server = ChatServer(login, password)
    server.on_connected = on_connected
    server.run()

    print 'uid: {}'.format(server.uid)

    try:
        print 'connect to uids: '
        while True:
            try:
                uid = raw_input('>')
                server.ask_to_connect(uid)
            except (KeyboardInterrupt, SystemExit):
                break

    except Exception as e:
        print 'ERROR!', repr(e)
    finally:
        server.stop()


if __name__ == '__main__':
    test()
