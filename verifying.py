import hashlib
import string
from datetime import datetime
from random import WichmannHill

from Crypto.Hash import SHA256
from Crypto.PublicKey import RSA


class UserAuthenticator:
    def __init__(self, login, password):
        self._login = login

        h = SHA256.new(login + '\n' + password).hexdigest()
        random = WichmannHill(h)

        def random_generator(n):
            return ''.join(random.choice(string.ascii_letters + string.digits) for _ in xrange(n))

        self.key = RSA.generate(1024, random_generator)

    @property
    def public_key(self):
        return self.key.publickey().exportKey('PEM')

    @property
    def uid(self):
        return hashlib.md5(self.public_key).hexdigest()

    @property
    def login(self):
        return self._login

    def sign(self, s):
        return self.key.sign(s, str(datetime.now()))


def verify_user(uid, public_key_str, test_str, signature):
    if hashlib.md5(public_key_str).hexdigest() != uid:
        return False

    key = RSA.importKey(public_key_str)
    public_key = key.publickey()

    return public_key.verify(test_str, signature)


def test():
    user = UserAuthenticator('user', '123456')
    print 'uid: {}'.format(user.uid)

    uid = user.uid
    public_key = user.public_key
    test_str = str(datetime.now())
    signature = user.sign(test_str)

    ok = verify_user(uid, public_key, test_str, signature)
    print 'OK' if ok else 'failed'

if __name__ == '__main__':
    test()
