from gui.widgets import *
import sys
import contacts


def main():
    app = QtGui.QApplication(sys.argv)

    connect_form = ConnectForm(contacts.get_contacts())
    login_form = LoginForm(connect_form)

    login_form.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
