import threading
from socket import *
import time

SEARCHING_PORT = 12312


def make_visible(uid, conn_func):
    t = _ListenThread(uid, conn_func)
    t.start()
    return t


class _ListenThread(threading.Thread):
    def __init__(self, uid, conn_func):
        super(_ListenThread, self).__init__(target=self._loop)

        self._uid = uid
        self._conn_func = conn_func
        self._stopped = False
        self._init_socket()

    def _init_socket(self):
        self._socket = s = socket(AF_INET, SOCK_DGRAM)
        s.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
        s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        s.settimeout(1)
        s.bind(('', SEARCHING_PORT))

    def _loop(self):
        while True:
            if self._stopped:
                break

            try:
                data, addr = self._socket.recvfrom(1024)
                find_uid, conn_port = self._parse_data(data)
            except (ValueError, timeout):
                continue

            if self._uid == find_uid:
                self._conn_func((addr[0], conn_port))

    @staticmethod
    def _parse_data(data):
        items = data.split('\n')
        if len(items) != 2:
            raise ValueError()

        return items[0], int(items[1])

    def stop(self):
        self._stopped = True


def search(uid, conn_port):
    s = socket(AF_INET, SOCK_DGRAM)
    s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

    data = '{}\n{}'.format(uid, str(conn_port))
    s.sendto(data, ('255.255.255.255', SEARCHING_PORT))


def test():
    def connected(addr):
        print 'connection received:', addr

    t = make_visible('1', connected)
    search('1', 12345)

    try:
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        t.stop()


if __name__ == '__main__':
    test()
