import json
import os.path

_CONTACTS_FILE = 'contacts.json'


def get_contacts():
    return _Contacts()


class _Contacts:
    def __init__(self):
        if not os.path.isfile(_CONTACTS_FILE):
            open(_CONTACTS_FILE, 'w').close()
        with open(_CONTACTS_FILE, 'r') as f:
            data = f.read()
            if data != '':
                self._items = json.loads(data)
            else:
                self._items = []

    def list(self):
        return self._items[:]

    def get(self, index):
        return self._items[index]

    def add(self, contact):
        self._items.append(contact)
        self._save()

    def remove(self, index):
        del self._items[index]
        self._save()

    def edit(self, index, contact):
        self._items[index] = contact
        self._save()

    def find_by_uid(self, uid):
        for item in self._items:
            if item[1] == uid:
                return item
        return None

    def _save(self):
        with open(_CONTACTS_FILE, 'w') as f:
            f.write(json.dumps(self._items))
