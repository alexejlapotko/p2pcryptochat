from PyQt4 import QtGui, uic, QtCore
import pyperclip

from PyQt4.QtGui import QListWidgetItem

from chat import ChatServer


def _center(self):
    self.move(QtGui.QApplication.desktop().screen().rect().center() - self.rect().center())


class LoginForm(QtGui.QWidget):
    def __init__(self, connect_form):
        QtGui.QWidget.__init__(self)
        uic.loadUi('gui/ui/login_form.ui', self)
        _center(self)

        self._connect_form = connect_form

        self.loginButton.clicked.connect(self._login_clicked)

    def _login_clicked(self):
        login = self.loginEdit.text()
        password = self.passwordEdit.text()

        self._connect_form.server = ChatServer(login, password)
        self.hide()
        self._connect_form.show()


class ConnectForm(QtGui.QWidget):
    _trigger = QtCore.pyqtSignal(object)

    def __init__(self, contacts):
        QtGui.QWidget.__init__(self)
        uic.loadUi('gui/ui/connect_form.ui', self)
        _center(self)

        self._server = None
        self._contacts = contacts

        self.connectButton.clicked.connect(self._ask_connect)
        self.addContact.clicked.connect(self._add_contact)
        self.editContact.clicked.connect(self._edit_contact)
        self.removeContact.clicked.connect(self._remove_contact)

    @property
    def server(self):
        return self._server

    @server.setter
    def server(self, server):
        self._server = server
        self.labelUid.setText(server.uid)
        self.copyButton.clicked.connect(self._copy_uid)
        self.setWindowTitle(server.login)

        server.on_connected = self._emit_on_connected
        self._trigger.connect(self._on_connected)

        server.run()

        self._show_contacts()

    def closeEvent(self, event):
        self.server.stop()
        super(ConnectForm, self).closeEvent(event)

    def _copy_uid(self):
        pyperclip.copy(self._server.uid)

    def _ask_connect(self):
        selected = self._get_selected()
        if selected is None:
            return
        uid = self._contacts.get(selected)[1]
        self.server.ask_to_connect(uid)

    def _emit_on_connected(self, connection):
        self._trigger.emit(connection)

    def _on_connected(self, connection):
        print 'connected ', connection

        contact = self._contacts.find_by_uid(connection.client_uid)
        if contact is None:
            contact = (connection.client_login, connection.client_uid)
            message = 'Client {} ({}) wants to connect you. Do you want add him to contacts and start chatting?'\
                .format(*contact)
            reply = QtGui.QMessageBox.question(self,
                                               'Connect request',
                                               message,
                                               QtGui.QMessageBox.Yes,
                                               QtGui.QMessageBox.No)

            if reply == QtGui.QMessageBox.Yes:
                form = ContactForm(contact, self)
                if form.exec_():
                    if form.contact is not None:
                        contact = form.contact
                        self._contacts.add(form.contact)
                        self._show_contacts()
            else:
                connection.close()
                return

        chat_form = ChatForm(connection, contact, self)
        chat_form.show()
        chat_form.exec_()

    def _add_contact(self):
        form = ContactForm(None, self)
        if form.exec_():
            if form.contact is not None:
                self._contacts.add(form.contact)
                self._show_contacts()

    def _edit_contact(self):
        selected = self._get_selected()
        if selected is None:
            return
        form = ContactForm(self._contacts.get(selected), self)
        if form.exec_():
            if form.contact is not None:
                self._contacts.edit(selected, form.contact)
                self._show_contacts()

    def _get_selected(self):
        l = self.contactsList.selectedIndexes()
        if len(l) < 1:
            return None
        return l[0].row()

    def _remove_contact(self):
        selected = self._get_selected()
        if selected is None:
            return
        self._contacts.remove(selected)
        self._show_contacts()

    def _show_contacts(self):
        self.contactsList.clear()
        for contact in self._contacts.list():
            self.contactsList.addItem(QListWidgetItem('{} ({})'.format(*contact)))


class ChatForm(QtGui.QDialog):
    _message_trig = QtCore.pyqtSignal(str)
    _disconnected_trig = QtCore.pyqtSignal(object)

    def __init__(self, conn, contact, parent=None):
        QtGui.QDialog.__init__(self, parent)
        uic.loadUi('gui/ui/chat_form.ui', self)

        self._conn = conn

        self.setWindowTitle('Chat with {} ({})'.format(*contact))

        self._message_trig.connect(self._on_message)
        self._disconnected_trig.connect(self._on_disconnected)

        conn.on_message_received = self._emit_on_message
        conn.on_disconnected = self._emit_on_disconnected

        self.sendButton.clicked.connect(self._send_message)

    def _send_message(self):
        text = self.messageEdit.toPlainText()
        self._conn.send_message(text)
        self._add_item('>> ' + text)
        self.messageEdit.setPlainText('')

    def _emit_on_message(self, text):
        self._message_trig.emit(text)

    def _on_message(self, text):
        self._add_item('<< ' + text)

    def _emit_on_disconnected(self, conn):
        self._disconnected_trig.emit(conn)

    def _on_disconnected(self, conn):
        self.sendButton.setEnabled(False)
        self.messageEdit.setEnabled(False)
        self.enterMessageLabel.setEnabled(False)

        self._add_item('client disconnected')

    def _add_item(self, text):
        self.messageList.addItem(QListWidgetItem(text))

    def closeEvent(self, event):
        self._conn.close()
        super(ChatForm, self).closeEvent(event)


class ContactForm(QtGui.QDialog):
    def __init__(self, contact, parent=None):
        QtGui.QDialog.__init__(self, parent)
        uic.loadUi('gui/ui/contact_form.ui', self)

        self._contact = contact

        if contact is None:
            self.setWindowTitle('Add contact')
        else:
            self.userNameEdit.setText(contact[0])
            self.uidEdit.setText(contact[1])
            self.setWindowTitle('Edit contact')

        self.okButton.clicked.connect(self._ok)
        self.cancelButton.clicked.connect(self._cancel)

    def _ok(self):
        self._contact = (str(self.userNameEdit.text()), str(self.uidEdit.text()))
        self.accept()

    def _cancel(self):
        self._contact = None
        self.reject()

    @property
    def contact(self):
        return self._contact
